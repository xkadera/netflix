<?php
session_start();
include_once "dtb_connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/app.css">
    <title>Netflix</title>
    <link rel="icon" href="images/small_netflix.png">
</head>
<body>

<?php
include "header.php";
include "billboard.php";
include "content.php";
include "footer.php";
?>





</body>
</html>