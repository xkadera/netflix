<div class="content-wrapper">
    <div class="billboard">
        <div class="small-title"> <img class="netflix-small" src="images/small_netflix.png" alt=""> SERIÁL </div>
        <h1>F1</h1>
        <h2>TOUHA <br> PO VÍTĚZSTVÍ</h2>
        <div class="btns-wrapper"> 
            <form action="#">
                <button class="btn-play"> <img class="btn-icons" src="images/play.png" alt=""> Přehrát</button>
            </form>
            <form action="#">
                <button class="btn-info"> <img class="btn-icons" src="images/info.png" alt=""> Další informace</button>
            </form>
            <a href="#"><img class="volume" src="images/volume.png" alt=""></a>
            <div class="year-limit">16+</div>
        </div>
    </div>