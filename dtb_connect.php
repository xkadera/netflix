<?php
function connect_dtb(){
    $server_name = "localhost";
    $user_name = "root";
    $password = "";
    $db_name = "netflix";
    
    $conn = new mysqli($server_name, $user_name, $password, $db_name);

    $conn->set_charset("utf8");
    return $conn;
}
?>