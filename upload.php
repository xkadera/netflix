<?php
session_start();
include_once "dtb_connect.php";

$name = htmlspecialchars($_POST["name"]);
$file = $_FILES["file"];
$category = $_POST["category"];
$date = new DateTime();

$path = "film_images/" . $date->getTimestamp() . $file["name"];
$file_type = explode("/", $file["type"])[0];

if (empty(basename($file["name"])) or empty($name)) {
    $_SESSION["error_message_film"] = "Nevyplněné pole.";
    header("Location: index.php#upload");
    exit();
}

if ($file_type != "image"){
    $_SESSION["error_message_film"] = "Vyberte obrázek.";
    header("Location: index.php#upload");
    exit();
}

$conn = connect_dtb();
if($conn->error){
    $_SESSION["error_message_film"] = "Nastala chyba.";
    exit();
}

if (!move_uploaded_file($file["tmp_name"], $path)){
    $_SESSION["error_message_film"] = "Chyba při nahrání";
    $conn->close();
    header("Location: index.php#upload");
    exit();
}

$sql = "INSERT INTO films (name, path, category) VALUES ('$name', '$path', '$category')";
if (!$conn->query($sql)) {
    $_SESSION["error_message_film"] = "Nastala chyba.";
    $conn->close();
    header("Location: index.php#upload");
    exit();
}


$conn->close();
$_SESSION["error_message_film"] = "Úspěšně nahráno";
header("Location: index.php#upload");
?>