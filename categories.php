<?php
function print_categories(){
    $conn = connect_dtb();
    if($conn->error){
        $_SESSION["error_message"] = "Nastala chyba.";
        return;
    }

    $sql = "SELECT id, name from categories";
    $result = $conn->query($sql);

    while($cur_row = $result->fetch_assoc()){
        echo "<option value='" . $cur_row["id"] . "'>" . $cur_row["name"] . "</option>";
    }
    $conn->close;
}

?>