<?php
session_start();
include_once "dtb_connect.php";

$user_email = htmlspecialchars($_POST["email"]);
$user_text = htmlspecialchars($_POST["user-text"]);
if (empty($user_email) or empty($user_text)) {
    $_SESSION["error_message"] = "Nevyplněné pole.";
    header("Location: index.php#contact-us");
    exit();
}

$conn = connect_dtb();
if($conn->error){
    $_SESSION["error_message"] = "Nastala chyba.";
    exit();
}

$sql = "INSERT INTO contact_us_table (email, user_text) VALUES ('$user_email', '$user_text')";


if (!$conn->query($sql)) {
    $_SESSION["error_message"] = "Nastala chyba.";
    $conn->close();
    header("Location: index.php#contact-us");
    exit();
}

$conn->close();
$_SESSION["error_message"] = "Úspěšně nahráno";
header("Location: index.php#contact-us");
?>