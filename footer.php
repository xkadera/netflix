<?php
include "categories.php"
?>
<footer>
<div class="left-footer">
<a href="" class="active">Domovská stránka</a>
<a href="">Pořady</a>
<a href="">Filmy</a>
<a href="">Nové a oblíbené</a>
<a href="">Můj seznam</a>
<div class="social-sites">
    <a href=""><img src="images/facebook.png"></a>
    <a href=""><img src="images/instagram.png"></a>
    <a href=""><img src="images/twitter.png"></a>
</div>
</div>



<form action="send_mail.php" method="post" id="contact-us">
    <h2>Kontaktujte nás!</h2>
    <input name="email" type="email" placeholder="Váš e-mail">
    <textarea name="user-text" rows="10" placeholder="Váš text"></textarea>
    <button type="submit">Odeslat</button>
    <?php
    if(isset($_SESSION["error_message"])){
        echo $_SESSION["error_message"];
    }
    $_SESSION["error_message"] = "";
    ?>
</form>

<form action="upload.php" method="post" id="upload" enctype="multipart/form-data">
    <h2>Vložte film!</h2>
    <input name="name" type="text" placeholder="Název filmu">
    <label class="category-label">Kategorie:</label>
    <select name="category">
        <?php
        print_categories();
        ?>
    </select>
    <input name="file" type="file">
    <button type="submit">Nahrát</button>
    <?php
    if(isset($_SESSION["error_message_film"])){
        echo $_SESSION["error_message_film"];
    }
    $_SESSION["error_message_film"] = "";
    ?>
</form>

</footer>