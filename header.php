<header>
    <a href="#"><img class="logo" src="images/netflix_logo.png" alt="netflix"> </a>
    <nav>
        <ul class="menu-links">
            <li><a href="" class="active">Domovská stránka</a></li>
            <li><a href="">Pořady</a></li>
            <li><a href="">Filmy</a></li>
            <li><a href="">Nové a oblíbené</a></li>
            <li><a href="">Můj seznam</a></li>
        </ul>
    </nav>
    <div class="search-bar" disabled>
        <input type="image" src="images/search.png" class="search-icon">
        <input class="search-input" type="text" placeholder="Hledej">
    </div>
    <a href="#"><img class="bell-icon" src="images/bell.png" alt=""></a>

    <div class="option-bar-wrapper">
        <div tabindex="1" class="option-bar">
            <img class="avatar-icon" src="images/avatar.png" alt="">
            <img class="triangle" src="images/triangle.png" alt="">
        </div>
        <div class="option-bar-content">
            <a href="#">Účet</a>
            <a href="#">Změna hesla</a>
        </div>
    </div>
    
</header> 